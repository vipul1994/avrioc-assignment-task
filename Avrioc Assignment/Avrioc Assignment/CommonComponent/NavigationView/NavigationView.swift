//
//  NavigationView.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//
import UIKit

class NavigationView: UIView {
    
    //MARK: IBOutlet and Constants
    @IBOutlet private (set) weak var backgroundView: UIView!
    @IBOutlet private (set) weak var leftBarView: UIView!
    @IBOutlet private (set) weak var leftBarImage: UIImageView!
    @IBOutlet private (set) weak var titleLabel: UILabel!
    @IBOutlet private (set) weak var searchView: UIView!
    @IBOutlet private (set) weak var moreView: UIView!
    
    var leftBarActionClouser: (()->())?
    var searchActionClouser: (()->())?
    var moreActionClouser: (()->())?
    
    //MARK: UIView Life Cyle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    //MARK: UIView Methods
    private func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
    }
    
    private func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: NavigationView.identifier, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func setUpUI(_ data: NavigationViewModel) {
        titleLabel.text = data.title
        leftBarImage.image = data.isSideMenu ?  #imageLiteral(resourceName: "img_menu") : #imageLiteral(resourceName: "img_back")
        searchView.isHidden = !data.isSearchShow
        moreView.isHidden = !data.isMoreShow
    }
    
    //MARK: IBAction
    @IBAction func leftBarAction(_ sender: Any) {
        leftBarActionClouser?()
    }
    
    @IBAction func searchAction(_ sender: Any) {
        searchActionClouser?()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        moreActionClouser?()
    }
}
