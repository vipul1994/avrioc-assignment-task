//
//  NavigationViewModel.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation

struct NavigationViewModel {
    let title: String?
    var isSideMenu: Bool = true
    var isSearchShow: Bool = true
    var isMoreShow: Bool = true
}
