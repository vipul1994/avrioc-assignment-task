//
//  BaseViewController.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
    }


}
