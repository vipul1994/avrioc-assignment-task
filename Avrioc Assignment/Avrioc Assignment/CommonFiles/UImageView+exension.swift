//
//  UImageView+exension.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 08/05/23.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImage(with url: String?, placeHolder: UIImage? = UIImage(named: "img_placeholder"), completed: (() -> Void)? = nil) {
        if let urlString = url {
            let url = URL(string: urlString)
            
            self.kf.setImage(with: url, placeholder: placeHolder, options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage,
            ], progressBlock: nil)
            
            
        } else {
            self.image = placeHolder
        }
    }
}
