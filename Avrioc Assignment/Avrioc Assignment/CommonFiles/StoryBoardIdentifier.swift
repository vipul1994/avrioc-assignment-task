//
//  StoryBoardIdentifier.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation

struct StoryBoardIdentifier {
    static let nyTimesList = "NYTimesList"
    static let nyTimesDetails = "NYTimesDetails"
    static let loader = "Loader"
}
