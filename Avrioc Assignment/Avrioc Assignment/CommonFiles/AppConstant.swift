//
//  AppConstant.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation
import UIKit

struct APIURL {
    static let nyTimeList = "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/7.json?api-key=\(APIConstant.apiKey)"
}

struct AppSingleton {
    static let appDelegate = UIApplication.shared.delegate  as? AppDelegate ?? AppDelegate()
}

struct APIConstant {
    static let apiKey = "23E6G948xA5MIB1xOvLtVCWbNKGjmWSa"
}
