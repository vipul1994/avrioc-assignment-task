//
//  GlobalUtility.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation
import UIKit

@objc class GlobalUtility: NSObject {
    
    static let shared: GlobalUtility = {
        let instance = GlobalUtility()
        return instance
    }()
    
    static func showHud() {
        let aStoryboard = UIStoryboard(name: StoryBoardIdentifier.loader, bundle: nil)
        let loaderVC = aStoryboard.instantiateViewController(withIdentifier: LoaderViewController.identifier) as! LoaderViewController
        let aParent = AppSingleton.appDelegate.window
        loaderVC.view.frame = UIScreen.main.bounds
        loaderVC.view.tag  = 10000
        aParent?.addSubview(loaderVC.view)
    }
    
    static func hideHud() {
        let aParent = AppSingleton.appDelegate.window
        for view in (aParent?.subviews)! {
            if view.tag == 10000
            {
                view.removeFromSuperview()
            }
        }
    }
}

