//
//  UIResponder+Extension.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation
import UIKit

extension UIResponder {
    
    static var identifier: String {
        return "\(self)"
    }
}
