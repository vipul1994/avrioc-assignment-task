//
//  StringConstants.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation

struct StringConstants {
    static let nyTimeList = "NYTimes List"
    static let nyTimeDetails = "NYTimes Details"
}
