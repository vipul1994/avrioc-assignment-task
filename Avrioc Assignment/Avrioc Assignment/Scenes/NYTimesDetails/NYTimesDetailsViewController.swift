//
//  NYTimesDetailsViewController.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import UIKit

class NYTimesDetailsViewController: UIViewController {

    //MARK: IBOutlet and Constants
    @IBOutlet private (set) weak var navigationView: NavigationView!
    @IBOutlet private (set) weak var iconImage: UIImageView!
    @IBOutlet private (set) weak var titleLabel: UILabel!
    @IBOutlet private (set) weak var descriptionLabel: UILabel!
    @IBOutlet private (set) weak var dateLabel: UILabel!
    var nyTimesListModel: NYTimeListResult?
    
    //MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }

    //MARK: Class Instance
    class func instance() -> NYTimesDetailsViewController? {
        return UIStoryboard(name: StoryBoardIdentifier.nyTimesDetails, bundle: nil).instantiateViewController(withIdentifier: NYTimesDetailsViewController.identifier) as? NYTimesDetailsViewController
    }
    
    //MARK: ViewController Methods
    private func setUp() {
        navigationView.setUpUI(NavigationViewModel(title: StringConstants.nyTimeDetails,isSideMenu: false, isSearchShow: false, isMoreShow: false))
        navigationView.leftBarActionClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        titleLabel.text = nyTimesListModel?.title
        descriptionLabel.text = nyTimesListModel?.adxKeywords
        dateLabel.text = nyTimesListModel?.publishedDate
        iconImage.setImage(with: nyTimesListModel?.media?.first?.mediaMetadata?.last?.url)
    }
}
