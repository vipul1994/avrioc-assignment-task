//
//  NYTimesViewModel.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation
class NYTimesViewModel {
    
    var apiService: ApiService = { return ApiService()}()
    var reloadTableViewClosure: (()->())?
    var numberOfRows: Int {
        return nyTimesListData.count
    }
    var nyTimesListData:[NYTimeListResult] = [NYTimeListResult]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    func getNYTimesList(response: @escaping(_ success: Bool, _ responseData: NYTimeListModel?) -> ()) {
        apiService.getNYTimesListResponse { success, responseData in
            response(success,responseData)
        }
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> NYTimeListResult {
        return nyTimesListData[indexPath.row]
    }
}
