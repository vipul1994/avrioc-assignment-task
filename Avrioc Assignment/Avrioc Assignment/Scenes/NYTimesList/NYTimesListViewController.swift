//
//  NYTimesListViewController.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import UIKit

class NYTimesListViewController: BaseViewController {
    
    //MARK: IBOutlet and Constants
    @IBOutlet private (set) weak var navigationView: NavigationView!
    @IBOutlet private (set) weak var listTable: UITableView!
    lazy var viewModel: NYTimesViewModel = {
        return NYTimesViewModel()
    }()

    var modelResponse : NYTimeListModel? {
        didSet {
            if let result = modelResponse?.results {
                nyTimesListData = result
            }
        }
    }
    var nyTimesListData = [NYTimeListResult]() {
        didSet {
            self.listTable.reloadData()
        }
    }
    
    //MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    //MARK: Class Instance
    class func instance() -> NYTimesListViewController? {
        return UIStoryboard(name: StoryBoardIdentifier.nyTimesList, bundle: nil).instantiateViewController(withIdentifier: NYTimesListViewController.identifier) as? NYTimesListViewController
    }
    
    //MARK: ViewController Methods
    private func setUp() {
        listTable.register(UINib(nibName: NYTimesListCell.identifier, bundle: nil), forCellReuseIdentifier: NYTimesListCell.identifier)
        getNYTimesListData()
        navigationView.setUpUI(NavigationViewModel(title: StringConstants.nyTimeList))
    }
}

//MARK: TableView Delegate and DataSource
extension NYTimesListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        nyTimesListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NYTimesListCell.identifier, for: indexPath) as? NYTimesListCell else { return UITableViewCell() }
        cell.setData(nyTimesListData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let nyTimesDetails = NYTimesDetailsViewController.instance() {
            nyTimesDetails.nyTimesListModel = nyTimesListData[indexPath.row]
            self.navigationController?.pushViewController(nyTimesDetails, animated: true)
        }
    }
}

//MARK: API Call
extension NYTimesListViewController {
    private func getNYTimesListData() {
        viewModel.getNYTimesList { success, responseData in
            if success {
                if let response = responseData {
                    self.modelResponse = response
                }
            }
        }
    }
}
