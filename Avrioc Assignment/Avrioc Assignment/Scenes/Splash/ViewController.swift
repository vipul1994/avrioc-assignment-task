//
//  ViewController.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let nyTimesListVC = NYTimesListViewController.instance() {
            self.navigationController?.pushViewController(nyTimesListVC, animated: true)
        }
    }

}

