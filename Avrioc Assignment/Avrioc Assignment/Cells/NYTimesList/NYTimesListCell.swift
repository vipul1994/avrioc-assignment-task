//
//  NYTimesListCell.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import UIKit

class NYTimesListCell: UITableViewCell {
    
    //MARK: IBOutlet and Constants
    @IBOutlet private (set) weak var iconImage: UIImageView!
    @IBOutlet private (set) weak var titleLabel: UILabel!
    @IBOutlet private (set) weak var descriptionLabel: UILabel!
    @IBOutlet private (set) weak var dateLabel: UILabel!
    
    //MARK: TableCell Life Cyle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: TableViewCell Method
    func setData(_ data: NYTimeListResult) {
        titleLabel.text = data.title
        descriptionLabel.text = data.adxKeywords
        dateLabel.text = data.publishedDate
        iconImage.setImage(with: data.media?.first?.mediaMetadata?.last?.url)
    }
}
