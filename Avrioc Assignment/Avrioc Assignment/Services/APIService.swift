//
//  APIService.swift
//  Avrioc Assignment
//
//  Created by Vipul  on 07/05/23.
//

import Foundation
import Alamofire

class ApiService {
    
    init() {}
    
    func getNYTimesListResponse(response: @escaping(_ success: Bool, _ responseData: NYTimeListModel?) -> ()) {
        GlobalUtility.showHud()
        AF.request(APIURL.nyTimeList).responseDecodable(of: NYTimeListModel.self) { serverResponse in
            guard let nyTimesList = serverResponse.value else {
                response(false, nil)
                GlobalUtility.hideHud()
                return
            }
            response(true, nyTimesList)
            GlobalUtility.hideHud()
        }
    }
}

