//
//  NYTimesListViewControllerTests.swift
//  Avrioc AssignmentTests
//
//  Created by Vipul  on 08/05/23.
//

import Foundation
import Nimble
import Quick

@testable import Avrioc_Assignment

class NYTimesListViewControllerTests: QuickSpec {
    var nyTimesListVC: NYTimesListViewController!
    
    override func spec() {
        describe("NYTimesListViewController") {
            context("when view is loaded with success response") {
                beforeEach {
                   // self.nyTimesListVC = NYTimesListViewController.stub()
                    
                    
                    let storyboard = UIStoryboard(name: StoryBoardIdentifier.nyTimesList, bundle: Bundle.main)
                    self.nyTimesListVC = storyboard.instantiateViewController(withIdentifier: NYTimesListViewController.identifier) as! NYTimesListViewController
                    _ = self.nyTimesListVC.view
                        if let path =  Bundle(for: type(of: self)
                        ).path(forResource: "NYTimesList", ofType: "json") {
                            do {
                                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                                let decoder = JSONDecoder()
                                decoder.keyDecodingStrategy = .convertFromSnakeCase
                                self.nyTimesListVC.modelResponse = try decoder.decode(NYTimeListModel.self, from: data)
                            } catch {
                                fail("Problem parsing JSON")
                            }
                        }
                    
                    //decode(NYTimesViewModel.self, from: "NYTimesList.json")
                }
                it("should all IBOutlets hooked up") {
                    expect(self.nyTimesListVC.listTable).notTo(beNil())
                    expect(self.nyTimesListVC.navigationView).notTo(beNil())
                }
                it("should confirm to tableview data source") {
                    expect(self.nyTimesListVC.listTable.dataSource?.conforms(to: UITableViewDataSource.self)).to(beTrue())
                    expect(self.nyTimesListVC.modelResponse).notTo(beNil())
                }
            }
        }
    }
}

extension NYTimesListViewController {
    static func stub() -> NYTimesListViewController? {
        guard let nyTimesListVC = UIStoryboard(name: StoryBoardIdentifier.nyTimesList, bundle:  nil).instantiateViewController(withIdentifier: NYTimesListViewController.identifier) as? NYTimesListViewController else {
            return nil
        }
        nyTimesListVC.simulateView(navigationController: NavController())
        return nyTimesListVC
    }
}

extension UIViewController{
    func simulateView(navigationController: UINavigationController = UINavigationController()) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        
        navigationController.viewControllers = [self]
        window.rootViewController = navigationController
        
        _ = self.view
        self.beginAppearanceTransition(true, animated: true)
        self.endAppearanceTransition()
        
    }
}

class NavController: UINavigationController {
    
    convenience init() {
        self.init(rootViewController: UIViewController())
        
    }
    
    override init (rootViewController: UIViewController) {
        super.init(rootViewController: UIViewController())
        self.viewControllers = [rootViewController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
    }
}


extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = dateDecodingStrategy
        decoder.keyDecodingStrategy = keyDecodingStrategy

        do {
            return try decoder.decode(T.self, from: data)
        } catch DecodingError.keyNotFound(let key, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
        } catch DecodingError.typeMismatch(_, let context) {
            fatalError("Failed to decode \(file) from bundle due to type mismatch – \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing \(type) value – \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(_) {
            fatalError("Failed to decode \(file) from bundle because it appears to be invalid JSON")
        } catch {
            fatalError("Failed to decode \(file) from bundle: \(error.localizedDescription)")
        }
    }
}
